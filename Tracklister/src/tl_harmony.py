'''
Created on 15.01.2020

@author: Daniel
'''
from _datetime import datetime
from requests_html import HTML, HTMLSession
from src import printer

days = ["today", "yesterday", "daybefore"]
name = "harmonyfm"
URL = "https://www.{}.de/musik/playlist/oController/Onair/oAction/showhitfinder.html".format(name)
#URL = "https://www.{}.de/musik/playlist/oController/Onair/oAction/additionalhitfinderitems/oFormat/json/onairchannelJson.html".format(harmony)



def run():
    scrape(URL)
    
def scrape(url):
    tracks = []
    print("working... This may take a while")
    i = 0
    for day in days:
        for hour in range(0,24):
    
            payload = {"tx_ffhonair_pi1[__referrer][@extension]":"FfhOnair",
                       "tx_ffhonair_pi1[__referrer][@vendor]":"RTFFH",
                       "tx_ffhonair_pi1[__referrer][@controller]":"Onair",
                       "tx_ffhonair_pi1[__referrer][@action]":"showhitfinder",
                       "tx_ffhonair_pi1[__referrer][arguments]":"YTo2OntzOjY6ImFjdGlvbiI7czoxMzoic2hvd2hpdGZpbmRlciI7czoxMDoiY29udHJvbGxlciI7czo1OiJPbmFpciI7czo3OiJzdGF0aW9uIjtzOjk6Imhhcm1vbnlmbSI7czo3OiJmb3JtZGF5IjtzOjU6InRvZGF5IjtzOjQ6ImhvdXIiO3M6MToiNyI7czo2OiJzdWJtaXQiO3M6NjoiU3VjaGVuIjt9402dc7a11f80852ff35883012175818f0a245cb9",
                       "tx_ffhonair_pi1[__referrer][@request]":'a:4:{s:10:"@extension";s:8:"FfhOnair";s:11:"@controller";s:5:"Onair";s:7:"@action";s:13:"showhitfinder";s:7:"@vendor";s:5:"RTFFH";}fcc68a79d980ebb9fecd30ec5464294ac583694b',
                       "tx_ffhonair_pi1[__trustedProperties]":'a:3:{s:7:"station";i:1;s:7:"formday";i:1;s:6:"submit";i:1;}eb619d7e7a02e676908fc4eb176dc03b69ccc8c0',
                       "tx_ffhonair_pi1[station]": "harmonyfm",
                       "tx_ffhonair_pi1[formday]":day,
                       "tx_ffhonair_pi1[hour]":hour,
                       "tx_ffhonair_pi1[submit]":"Suchen"}
            
            session = HTMLSession()
            r = session.get(url, params=payload)
            
            for e in r.html.find(".song"):
                i+=1
                if i % 500 == 0 :
                    print("still working... (I've found {} tracks so far!)".format(i))
                artist = e.find(".artist", first=True)
                title = e.find(".title", first=True)
                tracks.append((artist.text, title.text))
    
    printer.to_file(name, tracks)
